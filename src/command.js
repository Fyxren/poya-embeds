const { EmbedBuilder } = require("@discordjs/builders");
const { SlashCommandBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("test")
        .setDescription("I dare you to try this command!"),
    async execute(interaction) {
        let embed = new EmbedBuilder({
            image: {
                url: `https://media1.tenor.com/m/C3zfleKp-jYAAAAd/drinking-funny.gif`
            },
            color: 0xfab5d5
        });
        await interaction.reply({ embeds: [embed] });
    }
};