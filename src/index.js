require('dotenv').config();

/* Client setup */
const { Client, Events, GatewayIntentBits, REST, Routes } = require('discord.js');
const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent] });
const bypassUsers = ['1116903867586715758', '462914535351779328'];

client.once(Events.ClientReady, readyClient => {
  console.log(`Ready! Logged in as ${readyClient.user.tag}`);
});

/* Parse & send message */
client.on(Events.MessageCreate, message => {
  if (message.author.bot) return;
  if (message.content.startsWith('!') && (message.member.permissions.has('Administrator') || bypassUsers.includes(message.member.id))) {
    const toParse = message.content.slice(1);
    try {
      const data = JSON.parse(toParse);
      message.delete();
      message.channel.send(data).catch(e => {
        message.channel.send({ content: ':x: Invalid JSON' });
      });
    } catch (e) {
      return message.channel.send({ content: ':x: Invalid JSON' });
    }
  }
});

/* Command Handling */
const command = require('./command');
const rest = new REST().setToken(process.env.CLIENT_TOKEN);
try {
  rest.put(Routes.applicationGuildCommands(process.env.CLIENT_ID, process.env.GUILD_ID), { body: [command.data] });
  console.log('Reloaded application (/) commands');
} catch (e) {
  console.error(e);
}

client.on(Events.InteractionCreate, async interaction => {
  if (!interaction.isChatInputCommand()) return;

  try {
    await command.execute(interaction);
  } catch (error) {
    console.error(error);
    if (interaction.replied || interaction.deferred) {
      await interaction.followUp({ content: 'There was an error while executing this command!', ephemeral: true });
    } else {
      await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
    }
  }

});

/* Logging in */
client.login(process.env.CLIENT_TOKEN);