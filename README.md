# Poya Embeds
Hi Poya! This is a simple bot that sends embeds to a channel in your Discord server. You can create new embeds by using the [Discohook](https://discohook.org/) website and pasting the JSON data in the `data.json` file. The bot will then send the embed to the channel you specify.

_For a step-by-step guide, continue reading below :D_

## Installation
1. Run `npm i` to install dependencies
2. Rename `.env.example` to `.env` and fill in the required field
3. Run `npm run dev` to start the development server (restarts on file change)

## Creating a new embed
1. Create your desired message/embed using the [https://discohook.org/](https://discohook.org/)
2. At the bottom, click `JSON Data Editor` and `Copy to Clipboard`
3. Paste the data in the `data.json` file. Make sure to replace everything in the file with the new data.
4. Send `!` in the channel you want to send the embed in, the embed will now be sent and your message will be deleted.

5. Rinse and repeat for more embeds :D

<br />
<br />

<img src="https://i.pinimg.com/originals/50/cb/5c/50cb5cd8f238944acb1d09ff181a16e6.gif" width="400">
~ Fyxren

<br />
<br />

---

<br />

_I made this bot using `pnpm` instead of `npm`. It should still work, so I'm praying for it ;-;_ 